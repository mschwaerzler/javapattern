package at.sma.slick.stategamenew;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class GameOverState extends BasicGameState {

	@Override
	public void init(GameContainer arg0, StateBasedGame arg1)
			throws SlickException {
		// To Auto-generated method stub
		
	}

	@Override
	public void render(GameContainer arg0, StateBasedGame arg1, Graphics g)
			throws SlickException {
		// To Auto-generated method stub
		g.setColor(Color.white);
		g.drawString("State Over",300,300);
		g.drawString("Points earned " + GameSetup.gamescore,300,310);
		
	}

	@Override
	public void update(GameContainer arg0, StateBasedGame arg1, int arg2)
			throws SlickException {
		// To Auto-generated method stub
		
	}

	@Override
	public int getID() {
		// To Auto-generated method stub
		return 1;
	}



}
