package at.sma.slick.stategamenew;

import at.sma.slick.objects.GameCircle;
import at.sma.slick.objects.GameObject;
import at.sma.slick.objects.GameSquare;
import at.sma.slick.objects.GameTriangle;
import at.sma.slick.strategy.MoveLine;
import at.sma.slick.strategy.MoveSin;
import at.sma.slick.strategy.MoveZickZack;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Point;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import java.util.ArrayList;
import java.util.Random;

public class GameState extends BasicGameState {
	
	/***
	 *  Brauchen ein Liste von B�llen
	 */
	private ArrayList<GameObject> mObjects;
	private Circle mouseBall;
	private int timePassed;
	private Random random;

	/**
	 *
	 * @param container
	 * @param sbg
	 * @throws SlickException
	 *
	 * todo:  I want a ice cream
	 */
	@Override
	public void init(GameContainer container, StateBasedGame sbg)
			throws SlickException {
		// To Auto-generated method stub
		mObjects = new ArrayList<GameObject>();
		mouseBall = new Circle(0,0,10);
		timePassed =0;
		random = new Random();
		//mObjects.add(new GameSquare(300,0,10,Color.white,new MoveZickZack(2,2,true)));
		
	}

	@Override
	public void render(GameContainer container, StateBasedGame sbg, Graphics g)
			throws SlickException {
		// To Auto-generated method stub
		g.setColor(Color.white);
		//g.drawString("State 1",50,50);
		g.setColor(Color.yellow);
		g.fill(mouseBall);

		for(GameObject c : mObjects) {
			g.setColor(c.getaColor());
			switch(c.getaGameObjectName()) {
				case ("circle"):
					GameCircle gc = (GameCircle)c;
					g.fill(gc.getForm());
					break;
				case ("rectangle"):
					GameSquare gsc = (GameSquare)c;
					g.fill(gsc.getForm());
					break;
				case ("triangle"):
					GameTriangle gtc = (GameTriangle)c;
					g.fill(gtc.getForm());
					break;
			}
		}
		g.setColor(Color.red);
		g.drawString("Current mObjects : " + mObjects.size(),  10, 50);
		g.drawString("Current Lives : " + GameSetup.lives,  10, 65);
		g.drawString("Current Points : " + GameSetup.gamescore,  10, 80);
		//g.drawRect(30,30,10,10);
	}

		/***
		 * delta zeit die vom letzten mal vergangen sind
		 */
	@Override
	public void update(GameContainer container, StateBasedGame sbg, int delta)
			throws SlickException {
		// To Auto-generated method stub
		//** Falls action geschieht wechsle einfach in anderes Beispiel!
		if(container.getInput().isKeyPressed(Input.KEY_1)){
			//sbg.enterState(1);
			sbg.enterState(1, new FadeOutTransition(),new FadeInTransition());
		}
		mouseBall.setCenterX(container.getInput().getMouseX());
		mouseBall.setCenterY(container.getInput().getMouseY());
		timePassed += delta;
		if (timePassed > (200+random.nextInt(800))) {
			timePassed = 0;
			//mObjects.add(new GameCircle(200+random.nextInt(400),0,10,Color.blue));
			//mObjects.add(new GameSquare(100+random.nextInt(400),0,10,Color.white));
            int randomInt = random.nextInt(400);

			switch(random.nextInt(3)){
				case(0):
					mObjects.add(new GameCircle(200+randomInt,0,10,Color.blue, new MoveLine(false)));
					break;
				case(1):
					mObjects.add(new GameSquare(100+randomInt,0,10,Color.white,new MoveZickZack(0,1,true)));
					break;
				case(2):
					mObjects.add(new GameTriangle(100+randomInt,0,10,Color.white,new MoveSin()));
					break;
			}

		}

		/****
		 *    Move Objects with strategy
		 */
		for(GameObject c : mObjects) {
			c.move(delta);
		}
		/***
		 *    Check if Object is out of Limit or Livepoints are reduced
		 *
		 */
		for (int i = mObjects.size()-1; i >=0;i--) {
			GameObject c = mObjects.get(i);
			if(c.getaPositionY() > 610) {
				mObjects.remove(i);
				GameSetup.lives--;
			} else {

				switch(c.getaGameObjectName()) {
					case "circle":
						GameCircle gc = (GameCircle)c;
						if (gc.getForm().intersects(mouseBall)) {
							mObjects.remove(i);
							GameSetup.gamescore++;
						}
						break;
					case "rectangle":
						GameSquare gsc = (GameSquare)c;
						if (gsc.getForm().intersects(mouseBall)) {
							mObjects.remove(i);
							GameSetup.gamescore++;
						}
						break;
					case "triangle":
						GameTriangle gtc = (GameTriangle)c;
						if (gtc.getForm().intersects(mouseBall)) {
							mObjects.remove(i);
							GameSetup.gamescore++;
						}
						break;				}
			}
		}
		
		if (GameSetup.lives <=0) {
			sbg.enterState(1, new FadeOutTransition(),new FadeInTransition());
		}
	}

	/***
	 *  ID um zwischen den States wechseln
	 */
	@Override
	public int getID() {
		// To Auto-generated method stub
		return 0;
	}

}
