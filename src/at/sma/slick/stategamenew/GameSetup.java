package at.sma.slick.stategamenew;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class GameSetup extends StateBasedGame {


	public static int gamescore = 0;
	public static int lives = 3;
	
	public GameSetup(String name) {
		super(name);
		// To Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// To Auto-generated method stub
		try {
			AppGameContainer app = new AppGameContainer(new GameSetup("Setup Test"));
			app.setDisplayMode(800, 600, false);
			app.setTargetFrameRate(60);
			app.start();
		} catch (SlickException e) {
			// To Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void initStatesList(GameContainer container) throws SlickException {
		this.addState(new GameState());
		this.addState(new GameOverState());
		
		// To Auto-generated method stub
		
	}


}
