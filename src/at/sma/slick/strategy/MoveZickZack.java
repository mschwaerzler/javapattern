package at.sma.slick.strategy;

import org.newdawn.slick.geom.Point;

public class MoveZickZack implements MoveStrategy {

    private double mTime=0;
    private int mSpeed=0;
    private int mAngle=0;
    private double vx,vy;
    private double speedFactor = 0.01;
    private int startY = 1;

    public MoveZickZack( double t, int s,boolean upSize) {
        mTime =t;
        mSpeed =s;
        if (upSize) startY = -1;
    }


    public Point move(Point mpoint, double delta){
        int x;
        int y;
        mTime = mTime + delta;
        double t = speedFactor * mTime;
        /*
        if (startY > 0) {
            vx = 0.000001 * mSpeed;
            vy = Math.asin(Math.cos(t))*2/Math.PI * mSpeed * startY;
            x = (int) (mpoint.getX()+ (int)(vx* t));
            y = (int) (mpoint.getY()+(int)(vy));

        } else {

        */
            vy = 0.1 * mSpeed;
            vx = Math.asin(Math.cos(t))*2/Math.PI * mSpeed * startY;
            y = (int) (mpoint.getY()+(int)(vy* t));
            x = (int) (mpoint.getX()+(int)(vx*10));

        //}
        //System.out.println("x:" + x + " y:" + y + " mtime:" + mTime + " vy"+ vy + " vx"+vx);
        mpoint.setX(x);
        mpoint.setY(y);
        return mpoint;
    }
}
