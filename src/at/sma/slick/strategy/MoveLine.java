package at.sma.slick.strategy;

import org.newdawn.slick.geom.Point;


public class MoveLine implements MoveStrategy {
    private Boolean moveX;

    public MoveLine(Boolean moveX) {
        this.moveX = moveX;
    }

    public Point move(Point mPoint, double delta){
        if (!moveX)
            mPoint.setY(mPoint.getY()+ (int)(delta/5f));
        else
            mPoint.setX(mPoint.getX()+ (int)(delta/5f));
        return mPoint;
    }
}
