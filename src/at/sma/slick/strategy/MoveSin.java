package at.sma.slick.strategy;

import org.newdawn.slick.geom.Point;

public class MoveSin implements MoveStrategy {

    private double mTime=1;

    public MoveSin() {
    }


    public Point move(Point mpoint, double delta){
        mTime = mTime + delta/10f;
        mpoint.setX((int) (mpoint.getX() + (int) (Math.sin(mTime)*100)));
        mpoint.setY((int) (mpoint.getY() + (int)delta/5f));
        //if (mTime > 360) mTime = 1;
        System.out.println("mTime:"+mTime + " sinus: "+ (Math.sin(Math.toRadians(mTime))));
        return mpoint;
    }
}
