package at.sma.slick.strategy;

import org.newdawn.slick.geom.Point;

public interface MoveStrategy {
    public Point move(Point p, double delta);
}
