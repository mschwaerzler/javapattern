package at.sma.slick.objects;

import at.sma.slick.strategy.MoveStrategy;
import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Polygon;

public class GameTriangle extends GameObject {
    private Polygon aPoly;

    public GameTriangle(int positionX, int positionY, int speed, Color color, MoveStrategy ms) {
        super(positionX, positionY, speed, color, "triangle", ms);
        this.aPoly = new Polygon();
        this.aPoly.addPoint(-10,0);
        this.aPoly.addPoint(0,20);
        this.aPoly.addPoint(10,0);

    }

    public Polygon getForm() {
        aPoly.setCenterX(this.getaPositionX());
        aPoly.setCenterY(this.getaPositionY());
        return aPoly;
    }
}

