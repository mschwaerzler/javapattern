package at.sma.slick.objects;

import at.sma.slick.strategy.MoveStrategy;
import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Circle;

public class GameCircle extends GameObject {
    private Circle aCircle;

    public GameCircle(int positionX, int positionY, int speed, Color color, MoveStrategy ms) {
        super(positionX, positionY, speed, color, "circle", ms);
        this.aCircle = new Circle(positionX,positionY,10);
    }

    public Circle getForm() {
        aCircle.setCenterX(this.getaPositionX());
        aCircle.setCenterY(this.getaPositionY());
        return aCircle;
    }
}

