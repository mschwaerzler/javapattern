package at.sma.slick.objects;

import at.sma.slick.strategy.MoveStrategy;
import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Rectangle;

public class GameSquare extends GameObject {
    private Rectangle aRect;

    public GameSquare(int positionX, int positionY, int speed, Color color, MoveStrategy ms) {
        super(positionX, positionY, speed, color, "rectangle", ms);
        this.aRect = new Rectangle(positionX,positionY,10,10);
    }

    public Rectangle getForm() {
        aRect.setX(this.aPosition.getX());
        aRect.setY(this.aPosition.getY());
        return aRect;
    }
}

