package at.sma.slick.objects;

import at.sma.slick.strategy.MoveStrategy;
import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Point;

public abstract class GameObject {
    Point aPosition;
    int aSpeed;
    String aGameObjectName;
    MoveStrategy amoveStrategy;

    public String getaGameObjectName() {
        return aGameObjectName;
    }

    public MoveStrategy getAmoveStrategy() {
        return amoveStrategy;
    }

    public void setAmoveStrategy(MoveStrategy amoveStrategy) {
        this.amoveStrategy = amoveStrategy;
    }

    Color aColor;

    public GameObject(int positionX, int positionY, int speed, Color color, String gameObjectName,MoveStrategy ms) {

        this.aPosition = new Point(positionX,positionY);
        this.aSpeed = speed;
        this.aColor = color;
        this.aGameObjectName = gameObjectName;
        this.amoveStrategy = ms;

    }

    public GameObject(Point p, int speed, Color color, String gameObjectName,MoveStrategy ms) {

        aPosition = p;
        this.aSpeed = speed;
        aColor = color;
        aGameObjectName = gameObjectName;
        this.amoveStrategy = ms;

    }


    public int getaPositionX() {
        return (int) aPosition.getX();
    }

    public void setaPositionX(int aPositionX) {
        this.aPosition.setX(aPositionX);
    }

    public int getaPositionY() {
        return (int) aPosition.getY();
    }

    public void setaPositionY(int aPositionY) {
        this.aPosition.setY(aPositionY);
    }
    public void setaPosition(Point p) {
        this.aPosition = p;
    }

    public Point getaPosition() {
        return aPosition;
    }

    public int getaSpeed() {
        return aSpeed;
    }

    public void setaSpeed(int aSpeed) {
        this.aSpeed = aSpeed;
    }

    public Color getaColor() {
        return aColor;
    }

    public void setaColor(Color aColor) {
        this.aColor = aColor;
    }

    public void move(double delta) {
        Point p = amoveStrategy.move(this.aPosition,delta);
        this.setaPositionX((int) p.getX());
        this.setaPositionY((int) p.getY());
    }

}
